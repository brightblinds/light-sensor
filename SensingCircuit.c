/*
  Test Light Sensors.c (This is a work in progress) IN GENERAL 0 MEANS SHUT*****************
*/

#include "simpletools.h"


int main()                    
{
int lightLevel;
int sunThreshold = 2000;
int blindsOpen = 0;       //State of blinds 0 being shut 1 being open
int userChoice = 0; 	  //Manual override mode from PI (Will be modeled as push button or switch for now) 0 means shut
int timer = 0;			  //Timer of 0 means shut, 1 means open

int CHOICE_TIMER_SIM_BUTTON;
  
 
  
  
  
  while(1)																
  {
    high(9);
    pause(1);
    lightLevel = rc_time(9, 1);//Check light level when we drop out of a state I guess, I don't know if this counts as another state or not.
                               
    
   
    print("%cLight Level = %d Oh and your code is broken%c", //If you end up in this loop something went wrong.
           HOME, lightLevel, CLREOL);
    pause(250);
  
     
     
      while(lightLevel > sunThreshold && !blindsOpen ){      //State 1: Blinds opening****
         print("%cLight Level = %d%c",
           HOME, lightLevel, CLREOL);
		   
		   userChoice = 1; //Reset these every sunrise
		   timer = 1;
        
          print("%cGood Morning. Sending open blind command.%c", HOME, CLREOL);
           pause(5000); //Simulate opening sequence, in reality should check for an input from servo code to verify open
		   blindsOpen = 1; //Sets blinds to open and kicks us out of loop
          }
    
      while(timer && userChoice && blindsOpen){		//State 2: Blinds open****
          high(9);
          pause(1);
           lightLevel = rc_time(9, 1);//Read light level for display
        
          print("%cBlinds are open and sun is shining through! Light level = %d%c", HOME, lightLevel, CLREOL);
          CHOICE_TIMER_SIM_BUTTON = input(10);
          if (CHOICE_TIMER_SIM_BUTTON){ //Toggle user choice/timer when simulation button is pressed
              timer = !timer;
              userChoice = !userChoice;
              }    
  
           
          pause(250);
          
          }
          
      
       
       while((!userChoice || !timer) && blindsOpen ){		//State 3: Blinds shutting****
         
        
          print("%cSending shut blinds command%c", HOME, CLREOL);
          pause(5000); 	//Simulate shutting sequence, in reality should check for an input from Servo code to verify shut
		     blindsOpen = 0; //Set blinds to shut
          }
     
       while((!userChoice || !timer) && !blindsOpen && lightLevel < sunThreshold){		//State 4: Blinds shut****
        
        
          print("%cBlinds shut, enjoy your privacy. Light level = %d%c", HOME, lightLevel, CLREOL); //Display status of blinds
           high(9);
          pause(1);
         lightLevel = rc_time(9, 1); //check light level to see if blinds should open
          pause(250);
          }
   
          
             
   }//While(1)     
  }//Intmain
